package com.alexnae.base;

import org.bson.types.ObjectId;

public class Producto {
    private ObjectId _id;
    private String nombreProducto;
    private boolean salsa;
    private double precio;

    public Producto(String nombreProducto, boolean salsa, double precio) {
        this.nombreProducto = nombreProducto;
        this.salsa = salsa;
        this.precio = precio;
    }

    public Producto() {

    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public boolean isSalsa() {
        return salsa;
    }

    public void setSalsa(boolean salsa) {
        this.salsa = salsa;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }



    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Producto: " + nombreProducto +
                ", Salsa=" + salsa +
                ", Precio=" + precio;
    }
}
