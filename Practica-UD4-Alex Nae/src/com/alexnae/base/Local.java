package com.alexnae.base;

import org.bson.types.ObjectId;

public class Local {
    private ObjectId _id;
    private String nombreLocal;
    private String ciudad;
    private int codigoPostal;

    public Local(String nombreLocal, String ciudad, int codigoPostal) {
        this.nombreLocal = nombreLocal;
        this.ciudad = ciudad;
        this.codigoPostal = codigoPostal;
    }

    public Local() {

    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getNombreLocal() {
        return nombreLocal;
    }

    public void setNombreLocal(String nombreLocal) {
        this.nombreLocal = nombreLocal;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public int getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(int codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    @Override
    public String toString() {
        return "Local: " +
                nombreLocal +
                ", Ciudad='" + ciudad + '\'' +
                ", Código Postal=" + codigoPostal;
    }
}