package com.alexnae.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

public class Empleado {
    private ObjectId _id;
    private String nombre;
    private String apellidos;
    private LocalDate nacimiento;
    private Local local;

    public Empleado(String nombre, String apellidos, LocalDate nacimiento, Local local) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.nacimiento = nacimiento;
        this.local=local;
    }

    public Empleado() {

    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public LocalDate getNacimiento() {
        return nacimiento;
    }

    public void setNacimiento(LocalDate nacimiento) {
        this.nacimiento = nacimiento;
    }

    public Local getLocal() {
        return local;
    }

    public void setLocal(Local local) {
        this.local = local;
    }

    @Override
    public String toString() {
        return "Empleado: " + "Nombre: " + nombre + "," + apellidos + ", nacimiento: " + nacimiento;
    }
}
