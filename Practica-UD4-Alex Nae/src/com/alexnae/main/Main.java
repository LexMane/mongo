package com.alexnae.main;
/**
 * @author: Alex Nae
 */

import com.alexnae.gui.Controlador;
import com.alexnae.gui.Modelo;
import com.alexnae.gui.Vista;

/**
 * Clase Main
 */
public class Main {
    public static void main(String[] args) {new Controlador(new Modelo(), new Vista());
    }
}
