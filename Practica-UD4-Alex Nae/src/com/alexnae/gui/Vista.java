package com.alexnae.gui;


import com.alexnae.base.Empleado;
import com.alexnae.base.Local;
import com.alexnae.base.Producto;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

public class Vista extends JFrame {
    private JPanel panelPrincipal;

    // Productos
    JTextField txtNombreProducto;
    JTextField txtPrecioProducto;

    JList<Producto> listProductos;

    JButton btnAddProducto;
    JButton btnModProducto;
    JButton btnDelProducto;

    // Empleados
    JTextField txtNombreEmpleado;
    JTextField txtApellidosEmpleado;
    DatePicker dateNacimientoEmpleado;

    JList<Empleado> listEmpleados;

    JButton btnAddEmpleado;
    JButton btnModEmpleado;
    JButton btnDelEmpleado;

    // Locales
    JTextField txtNombreLocal;

    JList<Local> listLocales;

    JButton btnAddLocal;
    JButton btnModLocal;
    JButton btnDelLocal;

    JCheckBox cbSalsa;
     JTextField txtCiudadLocal;
     JTextField txtCodPostal;
     JComboBox comboLocales;
     JTextField txtBuscar;
     JList listBuscar;

    // Modelos
    DefaultListModel<Producto> dlmProductos;
    DefaultListModel<Empleado> dlmEmpleados;
    DefaultListModel<Local> dlmLocal;
    DefaultListModel<Producto> dlmBuscar;

    // Menu
    JMenuItem itemConectar;
    JMenuItem itemSalir;

    public Vista() {
        setTitle("Kebab Amigo - <SIN CONEXION>");
        setContentPane(panelPrincipal);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(800, 650));
        setResizable(false);
        pack();
        setVisible(true);

        inicializarModelos();
        inicializarMenu();
    }

    private void inicializarModelos() {
        dlmProductos = new DefaultListModel<>();
        listProductos.setModel(dlmProductos);
        dlmEmpleados = new DefaultListModel<>();
        listEmpleados.setModel(dlmEmpleados);
        dlmLocal = new DefaultListModel<>();
        listLocales.setModel(dlmLocal);
        dlmBuscar= new DefaultListModel<>();
        listBuscar.setModel(dlmBuscar);

    }

    private void inicializarMenu() {
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("conexion");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemSalir);

        JMenuBar menuBar = new JMenuBar();
        menuBar.add(menuArchivo);

        setJMenuBar(menuBar);
    }
}
