package com.alexnae.gui;



import com.alexnae.base.Empleado;
import com.alexnae.base.Local;
import com.alexnae.base.Producto;
import com.alexnae.util.Util;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;


/**
 * CLASE CONTROLADOR donde interactuaremos con la interfaz del usuario, es decir, el nexo entre Vista y Modelo
 */

public class Controlador implements ActionListener, ListSelectionListener, KeyListener {
    private Modelo modelo;
    private Vista vista;

    /**
     * Constructor de clase con Vista y Modelo mencionados
     * @param vista nuestra vista principal
     * @param modelo nuesto modelo
     */

    public Controlador(Modelo modelo, Vista vista) {
        this.vista = vista;
        this.modelo = modelo;

        addActionListeners(this);
        addListSelectionListeners(this);
        addKeyListener(this);

        try {
            modelo.conectar();
            vista.itemConectar.setText("Desconectar");
            vista.setTitle("Kebab Amigo - <CONECTADO>");
            setBotonesActivados(true);
            listarProductos();
            listarEmpleados();
            listarLocales();
            listarLocalesCB(modelo.getLocales());
        } catch (Exception ex) {
            Util.mostrarMensajeError("Imposible establecer conexión con el servidor.");
        }
    }

    private void addKeyListener(KeyListener listener) {
        vista.txtBuscar.addKeyListener(listener);
    }

    /**
     * Método que añade todos los listeners de la Vista
     * @param listener el listener que añadir (En este caso la clase hereda de Listener así que será this)
     */


    private void addActionListeners(ActionListener listener){
        vista.btnAddProducto.addActionListener(listener);
        vista.btnModProducto.addActionListener(listener);
        vista.btnDelProducto.addActionListener(listener);
        vista.btnAddEmpleado.addActionListener(listener);
        vista.btnModEmpleado.addActionListener(listener);
        vista.btnDelEmpleado.addActionListener(listener);
        vista.btnAddLocal.addActionListener(listener);
        vista.btnModLocal.addActionListener(listener);
        vista.btnDelLocal.addActionListener(listener);
        vista.itemConectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
    }



    /**
     * Método que permite registrar los cambios en las tablas mediante el conocido ListSelectionListener
     * @param listener el listener de la tabla a mirar (En nuestro caso this)
     */

    private void addListSelectionListeners(ListSelectionListener listener){
        vista.listProductos.addListSelectionListener(listener);
        vista.listEmpleados.addListSelectionListener(listener);
        vista.listLocales.addListSelectionListener(listener);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "conexion":
                try {
                    if (modelo.getCliente() == null) {
                        modelo.conectar();
                        vista.itemConectar.setText("Desconectar");
                        vista.setTitle("Kebab Amigo - <CONECTADO>");
                        setBotonesActivados(true);
                        listarProductos();
                        listarEmpleados();
                        listarLocales();
                    } else {
                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        vista.setTitle("Kebab Amigo - <SIN CONEXION>");
                        setBotonesActivados(false);
                        vista.dlmProductos.clear();
                        vista.dlmEmpleados.clear();
                        vista.dlmLocal.clear();
                        limpiarCamposProducto();
                        limpiarCamposEmpleado();
                        limpiarCamposLocal();
                    }
                } catch (Exception ex) {
                    Util.mostrarMensajeError("Imposible establecer conexión con el servidor.");
                }
                break;

            case "salir":
                modelo.desconectar();
                System.exit(0);
                break;

            case "addProducto":
                if (comprobarCamposProducto()) {
                    modelo.guardarObjeto(new Producto(vista.txtNombreProducto.getText(),
                            vista.cbSalsa.isSelected(),
                            Double.parseDouble(vista.txtPrecioProducto.getText())));
                } else {
                    Util.mostrarMensajeError("No ha sido posible insertar el producto en la base de datos.\n" +
                            "Compruebe que los campos contengan el tipo de dato requerido.");
                }
                listarProductos();
                break;

            case "modProducto":
                if (vista.listProductos.getSelectedValue() != null) {
                    if (comprobarCamposProducto()) {
                        Producto producto = vista.listProductos.getSelectedValue();
                        producto.setNombreProducto(vista.txtNombreProducto.getText());
                        producto.setSalsa(vista.cbSalsa.isSelected());
                        producto.setPrecio(Double.parseDouble(vista.txtPrecioProducto.getText()));
                        modelo.modificarObjeto(producto);
                    } else {
                        Util.mostrarMensajeError("No ha sido posible modificar el producto en la base de datos.\n" +
                                "Compruebe que los campos contengan el tipo de dato requerido.");
                    }
                    listarProductos();
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;

            case "delProducto":
                if (vista.listProductos.getSelectedValue() != null) {
                    modelo.eliminarObjeto(vista.listProductos.getSelectedValue());
                    listarProductos();
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;

            case "addEmpleado":
                if (comprobarCamposEmpleado()) {
                    modelo.guardarObjeto(new Empleado(vista.txtNombreEmpleado.getText(),
                            vista.txtApellidosEmpleado.getText(),
                            vista.dateNacimientoEmpleado.getDate(),
                            (Local) vista.comboLocales.getItemAt(vista.comboLocales.getSelectedIndex())));
                } else {
                    Util.mostrarMensajeError("No ha sido posible insertar el empleado en la base de datos.\n" +
                            "Compruebe que los campos contengan el tipo de dato requerido.");
                }
                listarEmpleados();
                break;

            case "modEmpleado":
                if (vista.listEmpleados.getSelectedValue() != null) {
                    if (comprobarCamposEmpleado()) {
                        Empleado empleado = vista.listEmpleados.getSelectedValue();
                        empleado.setNombre(vista.txtNombreEmpleado.getText());
                        empleado.setApellidos(vista.txtApellidosEmpleado.getText());
                        empleado.setNacimiento(vista.dateNacimientoEmpleado.getDate());
                        empleado.setLocal((Local) vista.comboLocales.getSelectedItem());
                        modelo.modificarObjeto(empleado);

                    } else {
                        Util.mostrarMensajeError("No ha sido posible modificar el empleado en la base de datos.\n" +
                                "Compruebe que los campos contengan el tipo de dato requerido.");
                    }
                    listarEmpleados();
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;

            case "delEmpleado":
                if (vista.listEmpleados.getSelectedValue() != null) {
                    modelo.eliminarObjeto(vista.listEmpleados.getSelectedValue());
                    listarEmpleados();
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;

            case "addLocal":
                if (comprobarCamposLocal()) {
                    modelo.guardarObjeto(new Local(vista.txtNombreLocal.getText(),
                            vista.txtCiudadLocal.getText(),
                            Integer.parseInt(vista.txtCodPostal.getText())));
                } else {
                    Util.mostrarMensajeError("No ha sido posible insertar el local en la base de datos.\n" +
                            "Compruebe que los campos contengan el tipo de dato requerido.");
                }
                listarLocales();
                break;

            case "modLocal":
                if (vista.listLocales.getSelectedValue() != null) {
                    if (comprobarCamposLocal()) {
                        Local local = vista.listLocales.getSelectedValue();
                        local.setNombreLocal(vista.txtNombreLocal.getText());
                        local.setCiudad(vista.txtCiudadLocal.getText());
                        local.setCodigoPostal(Integer.parseInt(vista.txtCodPostal.getText()));
                        modelo.modificarObjeto(local);
                    } else {
                        Util.mostrarMensajeError("No ha sido posible modificar el local en la base de datos.\n" +
                                "Compruebe que los campos contengan el tipo de dato requerido.");
                    }
                    listarLocales();
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;

            case "delLocal":
                if (vista.listLocales.getSelectedValue() != null) {
                    modelo.eliminarObjeto(vista.listLocales.getSelectedValue());
                    listarLocales();
                    break;
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
        }
        listarEmpleados();
        listarLocales();
        listarProductos();
        listarLocalesCB(modelo.getLocales());
    }



    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource() == vista.listProductos) {
            if (vista.listProductos.getSelectedValue() != null) {
                Producto producto = vista.listProductos.getSelectedValue();
                vista.txtNombreProducto.setText(producto.getNombreProducto());
                vista.cbSalsa.setSelected(producto.isSalsa());
                vista.txtPrecioProducto.setText(String.valueOf(producto.getPrecio()));
            }
        } else if (e.getSource() == vista.listEmpleados) {
            if (vista.listEmpleados.getSelectedValue() != null) {
                Empleado empleado = vista.listEmpleados.getSelectedValue();
                vista.txtNombreEmpleado.setText(empleado.getNombre());
                vista.txtApellidosEmpleado.setText(empleado.getApellidos());
                vista.dateNacimientoEmpleado.setDate(empleado.getNacimiento());
                vista.comboLocales.setSelectedItem(empleado.getLocal());
            }
        } else if (e.getSource() == vista.listLocales) {
            if (vista.listLocales.getSelectedValue() != null) {
                Local local = vista.listLocales.getSelectedValue();
                vista.txtNombreLocal.setText(local.getNombreLocal());
                vista.txtCiudadLocal.setText(local.getCiudad());
                vista.txtCodPostal.setText(String.valueOf(local.getCodigoPostal()));
            }
        }
    }

    private boolean comprobarCamposProducto() {
        return !vista.txtNombreProducto.getText().isEmpty() &&
                !vista.cbSalsa.getText().isEmpty() &&
                !vista.txtPrecioProducto.getText().isEmpty();
    }

    private boolean comprobarCamposEmpleado() {
        return !vista.txtNombreEmpleado.getText().isEmpty() &&
                !vista.txtApellidosEmpleado.getText().isEmpty() &&
                !vista.dateNacimientoEmpleado.getText().isEmpty();
    }

    private boolean comprobarCamposLocal() {
        return !vista.txtNombreLocal.getText().isEmpty()&&
                !vista.txtCiudadLocal.getText().isEmpty()&&
                !vista.txtCodPostal.getText().isEmpty();
    }

    /**
     * Limpia todos los campos de Producto
     */


    private void limpiarCamposProducto() {
        vista.txtNombreProducto.setText("");
        vista.cbSalsa.setText("");
        vista.txtPrecioProducto.setText("");
    }

    /**
     * Limpia todos los campos Empleado
     */

    private void limpiarCamposEmpleado() {
        vista.txtNombreEmpleado.setText("");
        vista.txtApellidosEmpleado.setText("");
        vista.comboLocales.setSelectedIndex(-1);
        vista.dateNacimientoEmpleado.clear();

    }

    /**
     * Limpia todos los campos Local
     */

    private void limpiarCamposLocal() {
        vista.txtNombreLocal.setText("");
        vista.txtCiudadLocal.setText("");
        vista.txtCodPostal.setText("");
    }


    /**
     * Lista todas las tablas de la Vista sacando los datos del modelo
     */

    private void listarProductos() {
        vista.dlmProductos.clear();
        for (Producto producto : modelo.getProductos()) {
            vista.dlmProductos.addElement(producto);
        }
    }

    private void listarEmpleados() {
        vista.dlmEmpleados.clear();
        for (Empleado empleado : modelo.getEmpleados()) {
            vista.dlmEmpleados.addElement(empleado);
        }
    }

    private void listarLocales() {
        vista.dlmLocal.clear();
        for (Local local : modelo.getLocales()) {
            vista.dlmLocal.addElement(local);
        }
    }

    private void listarLocalesCB(ArrayList<Local> lista){
        vista.dlmLocal.clear();
        for(Local local : lista){
            vista.dlmLocal.addElement(local);
        }

        vista.comboLocales.removeAllItems();
        ArrayList<Local> ins = modelo.getLocales();

        for (Local in : ins){
            vista.comboLocales.addItem(in);
        }
        vista.comboLocales.setSelectedIndex(-1);

    }

    private void buscarProducto(List<Producto> lista){
        vista.dlmBuscar.clear();

        for (Producto producto : lista){
            vista.dlmBuscar.addElement(producto);
        }
    }

    private void setBotonesActivados(boolean activados) {
        vista.btnAddProducto.setEnabled(activados);
        vista.btnModProducto.setEnabled(activados);
        vista.btnDelProducto.setEnabled(activados);
        vista.btnAddEmpleado.setEnabled(activados);
        vista.btnModEmpleado.setEnabled(activados);
        vista.btnDelEmpleado.setEnabled(activados);
        vista.btnAddLocal.setEnabled(activados);
        vista.btnModLocal.setEnabled(activados);
        vista.btnDelLocal.setEnabled(activados);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getSource() == vista.txtBuscar){
            buscarProducto(modelo.getProductos(vista.txtBuscar.getText()));
        }
    }
}
