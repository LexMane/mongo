package com.alexnae.gui;


import com.alexnae.base.Empleado;
import com.alexnae.base.Local;
import com.alexnae.base.Producto;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase Modelo
 */



public class Modelo {

    private MongoClient cliente;
    private MongoCollection<Document> productos;
    private MongoCollection<Document> empleados;
    private MongoCollection<Document> locales;

    public void conectar() {
        cliente = new MongoClient();
        String DATABASE = "KebabAmigo";
        MongoDatabase db = cliente.getDatabase(DATABASE);

        String COLECCION_PRODUCTOS = "Productos";
        productos = db.getCollection(COLECCION_PRODUCTOS);
        String COLECCION_EMPLEADOS = "Empleados";
        empleados = db.getCollection(COLECCION_EMPLEADOS);
        String COLECCION_LOCALES = "Locales";
        locales = db.getCollection(COLECCION_LOCALES);
    }

    public void desconectar() {
        cliente.close();
        cliente = null;
    }

    public MongoClient getCliente() {
        return cliente;
    }

    public ArrayList<Producto> getProductos() {
        ArrayList<Producto> lista = new ArrayList<>();

        for (Document document : productos.find()) {
            lista.add(documentToProducto(document));
        }
        return lista;
    }

    public ArrayList<Producto> getProductos(String comparador) {
        ArrayList<Producto> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("nombre", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : productos.find(query)) {
            lista.add(documentToProducto(document));
        }

        return lista;
    }

    public ArrayList<Empleado> getEmpleados() {
        ArrayList<Empleado> lista = new ArrayList<>();

        for (Document document : empleados.find()) {
            lista.add(documentToEmpleado(document));
        }
        return lista;
    }

    public ArrayList<Empleado> getEmpleados(String comparador) {
        ArrayList<Empleado> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("nombre", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("apellidos", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : empleados.find(query)) {
            lista.add(documentToEmpleado(document));
        }

        return lista;
    }

    public ArrayList<Local> getLocales() {
        ArrayList<Local> lista = new ArrayList<>();

        for (Document document : locales.find()) {
            lista.add(documentToLocal(document));
        }
        return lista;
    }

    public ArrayList<Local> getLocales(String comparador) {
        ArrayList<Local> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("local", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : locales.find(query)) {
            lista.add(documentToLocal(document));
        }

        return lista;
    }

    public void guardarObjeto(Object obj) {
        if (obj instanceof Producto) {
            productos.insertOne(objectToDocument(obj));
        } else if (obj instanceof Empleado) {
            empleados.insertOne(objectToDocument(obj));
        } else if (obj instanceof Local) {
            locales.insertOne(objectToDocument(obj));
        }
    }

    public void modificarObjeto(Object obj) {
        if (obj instanceof Producto) {
            Producto producto = (Producto) obj;
            productos.replaceOne(new Document("_id", producto.get_id()), objectToDocument(producto));
        } else if (obj instanceof Empleado) {
            Empleado empleado = (Empleado) obj;
            empleados.replaceOne(new Document("_id", empleado.get_id()), objectToDocument(empleado));
        } else if (obj instanceof Local) {
            Local local = (Local) obj;
            locales.replaceOne(new Document("_id", local.get_id()), objectToDocument(local));
        }
    }

    public void eliminarObjeto(Object obj) {
        if (obj instanceof Producto) {
            Producto producto = (Producto) obj;
            productos.deleteOne(objectToDocument(producto));
        } else if (obj instanceof Empleado) {
            Empleado empleado = (Empleado) obj;
            empleados.deleteOne(objectToDocument(empleado));
        } else if (obj instanceof Local) {
            Local local = (Local) obj;
            locales.deleteOne(objectToDocument(local));
        }
    }

    public Producto documentToProducto(Document dc) {
        Producto producto = new Producto();

        producto.set_id(dc.getObjectId("_id"));
        producto.setNombreProducto(dc.getString("nombre"));
        producto.setSalsa(dc.getBoolean("salsa"));
        producto.setPrecio(dc.getDouble("precio"));
        return producto;
    }

    public Empleado documentToEmpleado(Document dc) {
        Empleado empleado = new Empleado();

        empleado.set_id(dc.getObjectId("_id"));
        empleado.setNombre(dc.getString("nombre"));
        empleado.setApellidos(dc.getString("apellidos"));
        empleado.setNacimiento(LocalDate.parse(dc.getString("nacimiento")));
        return empleado;
    }

    public Local documentToLocal(Document dc) {
        Local local = new Local();
        local.set_id(dc.getObjectId("_id"));
        local.setNombreLocal(dc.getString("nombreLocal"));
        local.setCiudad(dc.getString("ciudad"));
        local.setCodigoPostal(dc.getInteger("codigopostal"));



        return local;
    }

    public Document objectToDocument(Object obj) {
        Document dc = new Document();

        if (obj instanceof Producto) {
            Producto producto = (Producto) obj;
            dc.append("nombre", producto.getNombreProducto());
            dc.append("salsa", producto.isSalsa());
            dc.append("precio", producto.getPrecio());
        } else if (obj instanceof Empleado) {
            Empleado empleado = (Empleado) obj;
            dc.append("nombre", empleado.getNombre());
            dc.append("apellidos", empleado.getApellidos());
            dc.append("nacimiento", empleado.getNacimiento().toString());

        } else if (obj instanceof Local) {
            Local local = (Local) obj;
            dc.append("nombreLocal", local.getNombreLocal());
            dc.append("ciudad", local.getCiudad());
            dc.append("codigopostal", local.getCodigoPostal());

        } else {
            return null;
        }
        return dc;
    }
}
